//
//  LuizalabsUITests.swift
//  LuizalabsUITests
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import XCTest

class LuizalabsUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
        XCUIDevice.shared().orientation = .portrait
        XCUIApplication().navigationBars["Desafio Luizalabs"].buttons["°C"].tap()
        
    }
    
    func testLaunchApp() {
        let app = XCUIApplication()
        XCTAssertTrue(app.navigationBars["Desafio Luizalabs"].buttons["°C"].exists)
        XCTAssertTrue(app.navigationBars["Desafio Luizalabs"].buttons["Mapa"].exists)
    }
    
    func testChangeLeftButton() {
        let app = XCUIApplication()
        app.navigationBars["Desafio Luizalabs"].buttons["°C"].tap()
        XCTAssertTrue(app.navigationBars["Desafio Luizalabs"].buttons["°F"].exists)
    }
    
    func testChangeRightButton() {
        let app = XCUIApplication()
        app.navigationBars["Desafio Luizalabs"].buttons["Mapa"].tap()
        XCTAssertTrue(app.navigationBars["Desafio Luizalabs"].buttons["Lista"].exists)
    }
    
}
