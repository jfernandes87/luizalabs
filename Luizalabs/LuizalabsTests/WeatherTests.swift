//
//  WeatherTests.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 23/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import XCTest
import Mapper
@testable import Luizalabs

let json1 = [
    "id": 3456944,
    "name": "Mongagua",
    "coord": [
        "Lat": -24.09306,
        "Lon": -46.620831
    ],
    "main": [
        "temp": 24.12,
        "temp_min": 21,
        "temp_max": 26,
        "pressure": 1018,
        "humidity": 64
    ],
    "weather": [
        [
            "id": 803,
            "main": "Clouds",
            "description": "nuvens quebrados",
            "icon": "04d"
        ]
    ]
] as NSDictionary

let coord = [
    "Lat": -24.09306,
    "Lon": -46.620831
] as NSDictionary

let main = [
    "temp": 24.12,
    "temp_min": 21,
    "temp_max": 26,
    "pressure": 1018,
    "humidity": 64
] as NSDictionary

let weather = [
    "id": 803,
    "main": "Clouds",
    "description": "nuvens quebrados",
    "icon": "04d"
]as NSDictionary

let json2 = [
    "id": 34569445,
    "name": "Mongagua",
    "coord": [
        "Lat": -25.09306,
        "Lon": -47.620831
    ],
    "main": [
        "temp": 24.12,
        "temp_min": 21,
        "temp_max": 26,
        "pressure": 1018,
        "humidity": 64
    ],
    "weather": [
        [
            "id": 803,
            "main": "Clouds",
            "description": "nuvens quebrados",
            "icon": "04d"
        ]
    ]
] as NSDictionary

let response = [
    "list": [json1, json2]
    ] as NSDictionary

class WeatherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testWeatherCollection() {
        do {
            let collection = try WeatherCollection(map: Mapper(JSON: response))
            XCTAssertEqual(collection.list.count, 2)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testWeather() {
        do {
            let weather = try Weather(map: Mapper(JSON: json1))
            XCTAssertEqual(weather.id, json1["id"] as! Int)
            XCTAssertEqual(weather.name, json1["name"] as! String)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testWeatherToCell() {
        do {
            let weather = try Weather(map: Mapper(JSON: json1))
            let cellController = weather.toCell(unit: Units.metric.rawValue)
            XCTAssertEqual(cellController.weather.name, json1["name"] as! String)
            XCTAssertEqual(cellController.unit, Units.metric.rawValue)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testWeatherImagePath() {
        do {
            let weather = try Weather(map: Mapper(JSON: json1))
            XCTAssertEqual(weather.imagePath(), URL(string:"http://openweathermap.org/img/w/04d.png"))
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testCoord() {
        do {
            let weather = try Coord(map: Mapper(JSON: coord))
            XCTAssertEqual(weather.lat, coord["Lat"] as! Double)
            XCTAssertEqual(weather.lon, coord["Lon"] as! Double)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testMain() {
        do {
            let weather = try Main(map: Mapper(JSON: main))
            XCTAssertEqual(weather.temp, main["temp"] as! Double)
            XCTAssertEqual(weather.tempMax, main["temp_max"] as! Double)
            XCTAssertEqual(weather.tempMin, main["temp_min"] as! Double)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func testInfo() {
        do {
            let info = try Info(map: Mapper(JSON: weather))
            XCTAssertEqual(info.icon , weather["icon"] as! String)
            XCTAssertEqual(info.description, weather["description"] as! String)
            XCTAssertEqual(info.id, weather["id"] as! Int)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
}
