//
//  Weather.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import Mapper

enum Units: String {
    case metric = "metric",
    imperial = "imperial"
}

class WeatherCollection: Mappable {
    
    private(set) var list: [Weather]
    
    required init(map: Mapper) throws {
        list = map.optionalFrom("list") ?? []
    }
}

class Weather: Mappable {
    
    private(set) var id: Int
    private(set) var name: String
    private(set) var coord: Coord
    private(set) var main: Main
    private(set) var info: [Info]
    
    required init(map: Mapper) throws {
        try id = map.from("id")
        try name = map.from("name")
        try coord = map.from("coord")
        try main = map.from("main")
        info = map.optionalFrom("weather") ?? []
    }
    
    func toCell(unit: String) -> WeatherCell {
        var instance: WeatherCell! = nil
        
        if let classInst = NSClassFromString("WeatherCell") as? WeatherCell.Type {
            instance = classInst.init(data: self, unit: unit)
        }
        
        return instance
    }
    
    func imagePath() -> URL {
        let pathURL = OpenWeatherManager.pathImage.replacingOccurrences(of: "{id}", with: self.info.first!.icon)
        
        return URL(string: pathURL)!
    }
}

class Main: Mappable {
   
    private(set) var temp: Double
    private(set) var tempMin: Double
    private(set) var tempMax: Double
    
    required init(map: Mapper) throws {
        try temp = map.from("temp")
        try tempMin = map.from("temp_min")
        try tempMax = map.from("temp_max")
    }
}

class Coord: Mappable {
    
    private(set) var lon: Double
    private(set) var lat: Double
    
    required init(map: Mapper) throws {
        try lon = map.from("Lon")
        try lat = map.from("Lat")
    }
    
}

class Info: Mappable {
    
    private(set) var id: Int
    private(set) var main: String
    private(set) var description: String
    private(set) var icon: String
    
    required init(map: Mapper) throws {
        try id = map.from("id")
        try main = map.from("main")
        try description = map.from("description")
        try icon = map.from("icon")
    }
}
