//
//  OpenWeatherManager.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import Mapper
import Alamofire

private let instance = OpenWeatherManager()

class OpenWeatherManager: NSObject {
    
    static let weatherNewValue = "weatherNewValue"
    static let weatherError = "weatherError"
    
    static let pathFind = "http://api.openweathermap.org/data/2.5/find"
    static let pathCity = "http://api.openweathermap.org/data/2.5/box/city"
    static let pathImage = "http://openweathermap.org/img/w/{id}.png"
    
    private(set) var weatherCollection: WeatherCollection? {
        didSet { OpenWeatherManager.weatherNewValue.notify() }
    }
    
    class var shared: OpenWeatherManager {
        return instance
    }
    
    func loadWeatherWithCoordinates(latitude: Double, longitude: Double, units: Units) {
        let parameters = toParameters(latitude: latitude, longitude: longitude, units: units)
        Alamofire.request(OpenWeatherManager.pathFind, parameters: parameters).responseJSON { response in
            if let json = response.result.value as? NSDictionary {
                do {
                    let result = try WeatherCollection(map: Mapper(JSON: json))
                    self.weatherCollection = result
                } catch let error as NSError {
                    OpenWeatherManager.weatherError.notifyWith(object: ["Message": error.localizedDescription] as AnyObject)
                }
            }
        }
    }
    
    func loadWeatherWithBounds(northEast: (lat: Double, lon: Double), southWest: (lat: Double, lon: Double), zoom: Int, units: Units) {
        let parameters = boundsToParameters(northEast: northEast, southWest: southWest, zoom: zoom, units: units)
        Alamofire.request(OpenWeatherManager.pathCity, parameters: parameters).responseJSON { response in
            if let json = response.result.value as? NSDictionary {
                do {
                    let result = try WeatherCollection(map: Mapper(JSON: json))
                    self.weatherCollection = result
                } catch let error as NSError {
                    OpenWeatherManager.weatherError.notifyWith(object: ["Message": error.localizedDescription] as AnyObject)
                }
            } else {
                OpenWeatherManager.weatherError.notifyWith(object: ["Message": "Sem informações no momento"] as AnyObject)
            }
        }
    }
    
    fileprivate func toParameters(latitude: Double, longitude: Double, units: Units) -> [String: Any] {
        return [
            "appid": "c64d5786fe9f193ef2c0de13d38bad17",
            "cnt": 20,
            "lang": "pt",
            "lat": latitude,
            "lon": longitude,
            "units": units.rawValue
        ]
    }
    
   fileprivate func boundsToParameters(northEast: (lat: Double, lon: Double), southWest: (lat: Double, lon: Double), zoom: Int, units: Units) -> [String: Any] {
        return [
            "appid": "c64d5786fe9f193ef2c0de13d38bad17",
            "lang": "pt",
            "bbox": "\(southWest.lon),\(southWest.lat),\(northEast.lon),\(northEast.lat),\(zoom)",
            "units": units.rawValue
        ]
    }
}
