//
//  AppHelper.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 23/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit

private let instance = AppHelper()

class AppHelper: NSObject {
    
    var geoloc = Geoloc()
    
    class var shared: AppHelper {
        return instance
    }
    
}
