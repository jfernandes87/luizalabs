//
//  Annotation.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 22/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject {
    var title: String?
    var image: URL?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D, image: URL) {
        self.title = title
        self.image = image
        self.coordinate = coordinate
        
        super.init()
    }
}

extension Annotation: MKAnnotation { }

extension Weather {
    func toAnnotation(unit: String) -> Annotation {
        let title = String(format: "%@ - %.0f %@", name, main.temp, unit)
        let coordinates = CLLocationCoordinate2D(latitude: self.coord.lat, longitude: self.coord.lon)
        
        return Annotation(title: title, coordinate: coordinates, image: imagePath())
    }
    
    func distanceToFromString(location: CLLocation) -> String {
        let distanceInMeters = location.distance(from: CLLocation(latitude: coord.lat, longitude: coord.lon))
        
        return String(format: "%.2f km", distanceInMeters/1000)
    }
    
    func distanceTo(location: CLLocation) -> Float {
        let distanceInMeters = location.distance(from: CLLocation(latitude: coord.lat, longitude: coord.lon))
        
        return Float(distanceInMeters)
    }
}
