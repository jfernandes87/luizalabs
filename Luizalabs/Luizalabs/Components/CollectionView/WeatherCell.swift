//
//  WeatherCell.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit

@objc(WeatherCell)
class WeatherCell: CellController {
    
    private(set) var weather: Weather
    private(set) var unit: String
    
    required init(data: Weather, unit: String) {
        self.weather = data
        self.unit = unit
        super.init()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = loadDefaultCellForCollection(collectionView: collectionView, atIndexPath: indexPath) as! WeatherCellView
        cell.city.text = cityMoreLocation()
        cell.weatherDescription.text = weather.info.first!.description
        cell.weatherValue.text = String(format:"%.0f %@", weather.main.temp, unit)
        cell.weatherMax.text = String(format:"%.0f %@", weather.main.tempMax, unit)
        cell.weatherMin.text = String(format:"%.0f %@", weather.main.tempMin, unit)
        cell.icon.setURL(url: weather.imagePath(), cache: true)
        
        return cell
    }
    
    func cityMoreLocation() -> String {
        guard let location = AppHelper.shared.geoloc.lastLocation else {
            return weather.name
        }
        
        return String(format: "%@ (%@)", weather.name, weather.distanceToFromString(location: location))
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width - 20, height: 200)
    }
    
}

class WeatherCellView: CellView {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var weatherValue: UILabel!
    @IBOutlet weak var weatherMax: UILabel!
    @IBOutlet weak var weatherMin: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.borderWidth = 1
        container.layer.cornerRadius = 8
        container.layer.borderColor = UIColor(netHex: 0x222222).cgColor
    }
}
