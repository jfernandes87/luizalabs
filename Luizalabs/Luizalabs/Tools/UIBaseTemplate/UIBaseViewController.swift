//
//  ButtonManager.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit

/**
 * UIBaseViewController
 * Facilita telas de loading e error, setando viewStatus
 * Implementa view padrão de loading e error
 * Você pode definir telas customizadas de loading e erro pelo IB
 * Automatiza o processo de download de dados e retry (via DownloadOccasion).
 */

var preloadedLoadingXib: UINib!
var preloadedExceptionsXib: UINib!

enum DownloadOccasion: Int {
    case viewDidLoad
    case viewWillAppear
    case viewDidAppear
    case retry
    case backFromBackground
}

enum UIBaseViewStatus: Int {
    case unknown
    case loading
    case presenting
    case exceptions
}

class UIBaseViewController: UIViewController {
    
    var viewStatus: UIBaseViewStatus?
    
    //MARK: - Outlet
    @IBOutlet weak var uiLoadingView: UIView?
    @IBOutlet weak var uiExceptionsView: UIView?
    
    deinit { print(description) }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoadingAndErrorView()
        tryDownloadingOnOccasion(occasion: .viewDidLoad)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tryDownloadingOnOccasion(occasion: .viewDidAppear)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLoadingAndErrorView()
        tryDownloadingOnOccasion(occasion: .viewWillAppear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: Download Stack
    private func tryDownloadingOnOccasion(occasion: DownloadOccasion) {
        if downloadData(viewOccasion: occasion) {
            setViewStatus(status: .loading, animated: false)
        }
    }
    
    func downloadData(viewOccasion: DownloadOccasion) -> Bool {
        return false
    }
    
    @IBAction func retry(sender: UIButton) {
        tryDownloadingOnOccasion(occasion: .retry)
    }
    
    private func changeActivity(isShow: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = isShow
    }
    
    //MARK: Status Stack
    private func setLastViewStatus() {
        setViewStatus(status: viewStatus ?? .presenting)
    }
    
    func setViewStatus(status: UIBaseViewStatus) {
        setViewStatus(status: status, animated: false)
    }
    
    func setViewStatus(status: UIBaseViewStatus, animated: Bool) {
        if animated {
            UIView.beginAnimations("UIBaseViewStatus", context: nil)
        }
        
        uiLoadingView?.alpha = 0.0
        uiExceptionsView?.alpha = 0.0
        
        if status == .exceptions {
            if let error = uiExceptionsView {
                view.bringSubview(toFront: error)
                error.alpha = 1.0
            }
        }
        
        if status == .loading {
            if let loading = uiLoadingView {
                changeActivity(isShow: true)
                view.bringSubview(toFront: loading)
                loading.alpha = 1.0
            }
        }
        
        if status == .presenting {
            changeActivity(isShow: false)
            if uiExceptionsView != nil && uiLoadingView != nil {
                uiLoadingView?.alpha = 0.0
                uiExceptionsView?.alpha = 0.0
            }
        }
        
        if(animated) {
            UIView.commitAnimations()
        }
    }
    
    //MARK: Setup Loading/ErrorView
    
    private func setupLoadingAndErrorView() {
        var statusViews = [UIView]()
        
        if uiLoadingView == nil {
            preloadedLoadingXib.instantiate(withOwner: self, options: nil)
            if let loading = uiLoadingView {
                statusViews.append(loading)
            }
        }
        
        if uiExceptionsView == nil {
            preloadedExceptionsXib.instantiate(withOwner: self, options: nil)
            if let error = uiExceptionsView {
                statusViews.append(error)
            }
        }
        
        for statusView in statusViews {
            if statusView.superview == nil {
                self.view.addSubview(statusView)
                statusView.frame = calculateViewBound(size: self.view.frame.size)
                statusView.clipsToBounds = true
                statusView.alpha = 0.0
                statusView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                statusView.setNeedsDisplay()
            }
        }
    }
    
    private func calculateViewBound(size: CGSize) -> CGRect {
        let bottom = tabBarController?.tabBar.frame.size.height ?? 0
        
        return CGRect(x: 0, y: 0, width: size.width, height: size.height - bottom)
    }
    
    private func calculateStatusBar(size: CGSize) -> CGFloat {
        let defaultSize = UIApplication.shared.statusBarFrame.size.height
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            return defaultSize
        } else {
            return size.width > size.height ? 0 : defaultSize
        }
    }
}
