//
//  CollectionManager.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit

@objc protocol CollectionManagerDelegate: NSObjectProtocol {
    @objc optional func collectionManager(_ collectionView: UICollectionView, didSelectItem item: CellController)
    @objc optional func collectionManager(_ collectionView: UICollectionView, didSelectIndexPath path: IndexPath)
    @objc optional func collectionManager(_ collectionView: UICollectionView, scrollView: UIScrollView)
    @objc optional func collectionManager(_ collectionView: UICollectionView, didEndScroll: UIScrollView)
}

enum CollectionManagerMode: Int {
    case singleSection
    case multipleSections
}

class CollectionManager: UICollectionView {
    
    var realod = true
    var defaultEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    var mode: CollectionManagerMode = .singleSection
    
    weak var managerDelegate: CollectionManagerDelegate?
    @IBOutlet weak var managerProtocol: AnyObject? {
        get {
            return managerDelegate
        }
        set {
            managerDelegate = newValue as? CollectionManagerDelegate
        }
    }
    
    var items = [AnyObject]() {
        willSet{
            self.items = newValue
            self.mode = .singleSection
            registerAllItens(items: newValue, reload: true)
        }
    }
    
    var sections = [AnyObject]()
    var sectionsAndItems = [AnyObject]()
    
    deinit {
        managerDelegate = nil
        print(description)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
        dataSource = self
        if #available(iOS 10.0, *) {
            prefetchDataSource = self
        }
    }
    
    override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CellView
        
        return cell
    }
    
    //MARK: Register Xib
    func registerAllItens(items:[AnyObject], reload: Bool) {
        
        for item in items {
            if let controller = item as? CellController, controller.isUsedNib == true {
                let nib = UINib(nibName: controller.reuseIdentifier(), bundle: nil)
                self.register(nib, forCellWithReuseIdentifier: controller.reuseIdentifier())
            }
        }
        
        if reload {
            self.reloadData()
        }
    }
    
    //MARK: Sections
    func setSectionsAndItems(sequence: [AnyObject]) {
        setSectionsAndItems(sequence: sequence, animated: true)
    }
    
    func setSectionsAndItems(sequence: [AnyObject], animated: Bool) {
        
        var haveSections = false
        
        for item in sequence {
            if let _ = item as? SectionController {
                haveSections = true
                break
            }
        }
        
        if haveSections == false {
            items = sequence
            
            return
        }
        
        registerAllItens(items: sequence, reload: false)
        
        var _items = [AnyObject]()
        var _newSections = [AnyObject]()
        var _cleanSecAndItems = [AnyObject]()
        var currentSection: SectionController?
        
        for (index, obj) in sequence.enumerated() {
            if let item = obj as? CellController {
                _items.append(item)
            }
            
            if let item = obj as? SectionController {
                if currentSection != nil {
                    currentSection?.items = _items as [AnyObject]?
                    _cleanSecAndItems.append(contentsOf: _items)
                    _newSections.append(currentSection!)
                }
                
                _items.removeAll()
                _items = [AnyObject]()
                currentSection = item
                _cleanSecAndItems.append(currentSection!)
                
            }
            
            if index + 1 == sequence.count {
                if currentSection != nil {
                    currentSection?.items = _items as [AnyObject]?
                    _cleanSecAndItems.append(contentsOf: _items)
                    _newSections.append(currentSection!)
                }
                
                if let item = obj as? SectionController {
                    _items.removeAll()
                    _items = [AnyObject]()
                    currentSection = item
                    _cleanSecAndItems.append(currentSection!)
                }
            }
        }
        
        sectionsAndItems.removeAll(keepingCapacity: false)
        sectionsAndItems = _cleanSecAndItems
        
        sections.removeAll(keepingCapacity: false)
        sections = _newSections
        
        mode = .multipleSections
        
        if animated {
            reloadData()
        }
    }
    
    //MARK: scrolling stuff
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isScrollEnabled {
            super.touchesMoved(touches, with: event)
        }
    }
    
    //MARK: rows manipulation
    
    func markCellAsSelected(cell: CellController) {
        let path = indexPathForCellController(cell: cell)
        selectItem(at: path, animated: false, scrollPosition: .centeredVertically)
    }
    
    func removeAt(position: Int, rowsCount count: Int) {
        var paths = [IndexPath]()
        let allRows = NSMutableArray(array: items)
        var discardRows = [AnyObject]()
        
        for i in position ..< position + count {
            let cellController = items[i] as! CellController
            discardRows.append(cellController)
            
            let path: IndexPath = indexPathForCellController(cell: cellController)!
            paths.append(path)
        }
        
        allRows.removeObjects(in: discardRows)
        
        performBatchUpdates({
            self.items.removeAll(keepingCapacity: false)
            self.items = allRows as [AnyObject]
            
            self.deleteItems(at: paths)
        }) { (success) in
            
        }
    }
    
    func remove(rows: [CellController]) {
        var paths = [IndexPath]()
        
        for controller in rows {
            if let path: IndexPath = indexPathForCellController(cell: controller) {
                paths.append(path)
            }
        }
        
        if mode == .singleSection {
        
            let allRows = NSMutableArray(array: items)
            allRows.removeObjects(in: rows)
        
            performBatchUpdates({
                self.items.removeAll(keepingCapacity: false)
                self.items = allRows as [AnyObject]
                self.deleteItems(at: paths)
            }) { (success) in
            
            }
            
        } else {
            
            let allRows = NSMutableArray(array: sectionsAndItems)
            allRows.removeObjects(in: rows)
            
            performBatchUpdates({
                self.setSectionsAndItems(sequence: allRows as [AnyObject])
                self.deleteItems(at: paths)
            }) { (success) in
                
            }
            
        }
    }
    
    func insertRowsOnTop(rows: [AnyObject]) {
        insertRows(_rows: rows, at: 0)
    }
    
    func insertRows(_rows: [AnyObject], at position: Int) {
        
        var newRows = [AnyObject]()
        
        let allRows = NSMutableArray(array: mode == .singleSection ? items : sectionsAndItems)
    
        for i in (0..<_rows.count).reversed() {
            if let cellController = _rows[i] as? CellController {
                allRows.insert(cellController, at: position)
                newRows.append(cellController)
            }
        }
        
        if mode == .singleSection {
            items.removeAll(keepingCapacity: false)
            items = allRows as [AnyObject]
        } else {
            setSectionsAndItems(sequence: allRows as [AnyObject])
        }
        
        //setContentOffset(CGPoint(x: self.contentOffset.x, y: self.contentOffset.y), animated: false)
    }
    
    func findControllerAtIndexPath(indexPath: IndexPath) -> CellController {
        var controller: CellController?
        if mode == .singleSection {
            if items.count > indexPath.row {
                controller = items[indexPath.row] as? CellController
            }
        } else {
            if let section = sections[indexPath.section] as? SectionController, let rows = section.items {
                controller = rows[indexPath.row] as? CellController
            }
        }
        
        return controller!
    }
    
    func indexPathForCellController(cell: CellController) -> IndexPath? {
        if mode == .multipleSections {
            for (i, aSection) in sections.enumerated() {
                if let section = aSection as? SectionController, let rows = section.items {
                    for (j, aCell) in rows.enumerated() {
                        if let aCell = aCell as? CellController {
                            if aCell == cell {
                                return IndexPath(row: j, section: i)
                            }
                        }
                    }
                }
            }
        } else {
            for (index, aCell) in items.enumerated() {
                if let aCell = aCell as? CellController {
                    if(aCell == cell) {
                        return IndexPath(row: index, section: 0)
                    }
                }
            }
        }
        
        return nil
    }
}

@available(iOS 10.0, *)
extension CollectionManager: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let controller = findControllerAtIndexPath(indexPath: indexPath)
            controller.collectionView = collectionView
            controller.collectionView(collectionView, prefetchItem: indexPath)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        
    }
}

extension CollectionManager: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if mode == .singleSection { return 1
        } else { return sections.count }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mode == .singleSection { return items.count
        } else {
            if sections.count == 0 { return 0 }
            if let controller = sections[section] as? SectionController {
                return controller.items!.count
            }
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let controller = findControllerAtIndexPath(indexPath: indexPath)
        controller.collectionView = collectionView
        
        return controller.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let controller = findControllerAtIndexPath(indexPath: indexPath)
        controller.collectionView = collectionView
        
        return controller.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if sections.count == 0 { return defaultEdgeInsets
        } else if let sectionController = sections[section] as? SectionController { return sectionController.edgeInsets
        } else { return defaultEdgeInsets }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = findControllerAtIndexPath(indexPath: indexPath)
        guard let delegate = self.managerDelegate else { return }
        
        if delegate.responds(to: #selector(CollectionManagerDelegate.collectionManager(_:didSelectItem:))) {
            delegate.collectionManager!(self, didSelectItem: controller)
        }
        
        if delegate.responds(to: #selector(CollectionManagerDelegate.collectionManager(_:didSelectIndexPath:))) {
            delegate.collectionManager!(self, didSelectIndexPath: indexPath)
        }
        
        return controller.collectionView(collectionView, didSelectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let controller = findControllerAtIndexPath(indexPath: indexPath)
        controller.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
    }
}

extension CollectionManager: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let delegate = self.managerDelegate else { return }
        if delegate.responds(to: #selector(CollectionManagerDelegate.collectionManager(_:scrollView:))) {
            delegate.collectionManager!(self, scrollView: scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let delegate = self.managerDelegate else { return }
        if delegate.responds(to: #selector(CollectionManagerDelegate.collectionManager(_:didEndScroll:))) {
            delegate.collectionManager!(self, didEndScroll: scrollView)
        }
    }
}

//MARK: Cell Controller

@objc class CellController: NSObject {
    
    @IBOutlet weak var controllerCell: CellView!
    
    var collectionView: UICollectionView?
    var tag: Int?
    var identifier: String?
    var currentObject: AnyObject?
    var isUsedNib = false
    
    override init() {
        super.init()
    }
    
    convenience init(nib: Bool) {
        self.init()
        isUsedNib = nib
    }
    
    func reloadMe() {
        if let index: IndexPath = (collectionView as! CollectionManager).indexPathForCellController(cell: self) {
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadItems(at: [index])
            }, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return loadDefaultCellForCollection(collectionView: collectionView, atIndexPath: indexPath) as! CellView
    }
    
    func reuseIdentifier() -> String {
        return NSStringFromClass(object_getClass(self))
    }
    
    func loadDefaultCellForCollection(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> AnyObject {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier(), for: indexPath) as? CellView
        
        if cell == nil {
            let _ = CollectionManagerCache.shared().loadNib(path: reuseIdentifier(), owner: self)
            cell = controllerCell;
            controllerCell = nil
        }
        
        cell!.controller = self
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return UIScreen.main.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItem indexPath:IndexPath) {
        
    }
    
    deinit{
        collectionView = nil
        controllerCell = nil
    }
}

class CellView: UICollectionViewCell {
    
    var loadedKey: String?
    
    @IBOutlet weak var controller: CellController!
    @IBOutlet weak var backgroundCell: UIView!
    
    deinit{
        controller = nil
        loadedKey = nil
    }
}

class SectionController: NSObject {
    
    var items: [AnyObject]?
    var edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    
    convenience init(insets: UIEdgeInsets) {
        self.init()
        edgeInsets = insets
    }
    
    deinit{ items = nil }
}
