//
//  CollectionManagerCache.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit

class CollectionManagerCache: NSObject {
    var items = NSMutableDictionary()
    
    static var instance: CollectionManagerCache!
    
    deinit { NSLog("Dealloc CollectionManagerCache") }
    
    class func shared() -> CollectionManagerCache {
        self.instance = (self.instance ?? CollectionManagerCache())
        
        return self.instance
    }
    
    override init() {
        super.init()
        let selector = #selector(CollectionManagerCache.clearCache)
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name.UIApplicationDidReceiveMemoryWarning, object: nil)
    }
    
    func loadNib(path: String?, owner: AnyObject) -> Any? {
        guard let pathItem = path else {
            return nil
        }
        
        var cached: UINib?
        objc_sync_enter(items)
        cached = items.object(forKey: pathItem) as? UINib
        objc_sync_exit(items)
        
        if let cachedNib = cached {
            return cachedNib.instantiate(withOwner: owner, options: [:])
        } else {
            let newNib = UINib(nibName: pathItem, bundle: nil)
            objc_sync_enter(items)
            items.setValue(newNib, forKey: pathItem)
            objc_sync_exit(items)
            
            return newNib.instantiate(withOwner: owner, options: [:])
        }
    }
    
    func clearCache() {
        items.removeAllObjects()
    }
}
