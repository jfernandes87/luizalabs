//
//  String+Helpers.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import MapKit

extension String {
    func notify() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: self), object: nil)
    }
    
    func notifyWith(object: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: self), object: object)
    }
}

extension NSObject {
    func listenTo(notificationName: String, call selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    func stopListening(notificationName: String) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
}

extension MKMapView {
    func zoom() -> Int {
        var angleCamera = self.camera.heading
        
        if angleCamera > 270 {
            angleCamera = 360 - angleCamera
        } else if angleCamera > 90 {
            angleCamera = fabs(angleCamera - 180)
        }
        
        let angleRad = Double.pi * angleCamera / 180 // camera heading in radians
        let width = Double(self.frame.size.width)
        let height = Double(self.frame.size.height)
        let heightOffset : Double = 20 // the offset (status bar height) which is taken by MapKit into consideration to calculate visible area height
        
        // calculating Longitude span corresponding to normal (non-rotated) width
        let spanStraight = width * self.region.span.longitudeDelta / (width * cos(angleRad) + (height - heightOffset) * sin(angleRad))
        
        return Int(log2(360 * ((width / 256) / spanStraight)) + 1)
    }
}
