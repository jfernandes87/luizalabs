//
//  UIImageView+Network.swift
//  Enjoei
//
//  Created by Julio Fernandes on 16/07/16.
//  Copyright © 2016 Julio Fernandes. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setURL(url: URL?) {
        setURL(url: url, cache: true)
    }
    
    func setURL(url: URL?, cache: Bool) {
        self.image = nil
        OperationQueue().cancelAllOperations()
        OperationQueue().addOperation({
            self.downloadImg(url: url, cache: true)
        })
    }
    
    func downloadImg(url: URL?, cache: Bool) {
        
        guard let pathURL = url else {
            self.image = nil
            return
        }
        
        var data: NSData!
        
        if(!cache) {
            data = NSData(contentsOf: pathURL)
        } else {
            
            let path = relativePath(pathURL: pathURL)
            
            if( FileManager.default.fileExists(atPath: path)){
                data = NSData(contentsOfFile: path)
            } else {
                data = NSData(contentsOf: pathURL)
                if data != nil {
                    data.write(toFile: path, atomically: true)
                }
            }
        }
        
        OperationQueue.main.addOperation {
            self.showImg(data: data)
        }
    }
    
    func relativePath(pathURL: URL) -> String {
        var path = pathURL.absoluteString.replacingOccurrences(of: "/", with: "_")
        path = path.replacingOccurrences(of: "\\", with: "_")
        path = path.replacingOccurrences(of: ":", with: "_")
        
        return "\(NSHomeDirectory())/Documents/\(path)"
    }
    
    func showImg(data: NSData?) {
        if let data = data, data.length > 0 {
            self.image = UIImage(data:data as Data)
        } else {
            self.image = nil
        }
    }
}
