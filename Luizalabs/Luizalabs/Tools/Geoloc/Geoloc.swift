//
//  Geoloc.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright (c) 2015 Zap Imóveis. All rights reserved.
//

import UIKit
import CoreLocation

enum GeolocFailReason : Int {
    case unknown
    case authorizationRestricted
    case authorizationDenied
    case locationDisabled
    case locationDisabledInsideApp
    case networkError
}

@objc protocol GeolocDelegate: NSObjectProtocol {
    @objc optional func geoloc(geoLoc: Geoloc, didGetUserLocation location: CLLocation)
    @objc optional func geoloc(geoLoc: Geoloc, failWithReason reason: Int, message: String)
}

class Geoloc: NSObject, CLLocationManagerDelegate {
    
    var manager: CLLocationManager?
    var lastLocation: CLLocation?
    var forceUpdateAtualLocation = false
    var delegate: GeolocDelegate?
    
    func userLocationRequiredWithDelegate(delegate: GeolocDelegate) {
        self.delegate = delegate
        requireLocation()
    }
    
    func userLocationReviewRequired() {
        requireLocation()
    }
    
    func requireLocation() {
        if manager == nil {
            manager = ({
                let locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.distanceFilter = kCLDistanceFilterNone
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestWhenInUseAuthorization()
                
                return locationManager
            })()
        }
        
        if lastLocation != nil {
            sendLocationToDelegate()
        } else {
            if let locationManager = manager {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func stopLocationRequests() {
        if let locationManager = manager {
            locationManager.stopUpdatingLocation()
        }
    }
    
    //MARK: delegate response
    private func sendLocationToDelegate() {
        if let location = lastLocation, let delegate = delegate, delegate.responds(to: #selector(GeolocDelegate.geoloc(geoLoc:didGetUserLocation:))) {
            delegate.geoloc!(geoLoc: self, didGetUserLocation: location)
        }
        stopLocationRequests()
    }
    
    private func sendFailToDelegateWithReason(reason: GeolocFailReason, message: String) {
        if let delegate = delegate, delegate.responds(to: #selector(GeolocDelegate.geoloc(geoLoc:failWithReason:message:))) {
            delegate.geoloc!(geoLoc: self, failWithReason: reason.hashValue, message: message)
        }
        stopLocationRequests()
    }
    
    //MARK: status
    func userLocationAvailable() -> Bool {
        if !CLLocationManager.locationServicesEnabled() { return false }
        if CLLocationManager.authorizationStatus() == .denied { return false }
        if CLLocationManager.authorizationStatus() == .restricted { return false }
        
        return true;
    }
    
    //MARK: Location manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            stopLocationRequests()
            
            return
        }
        
        if location.timestamp.timeIntervalSinceNow > -10.0 || forceUpdateAtualLocation == true {
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            lastLocation = location
            forceUpdateAtualLocation = false
            sendLocationToDelegate()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                self.useCachedLocation(loc: location)
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let error = CLError(_nsError: error as NSError)
        switch error.code {
            case .network: sendFailToDelegateWithReason(reason: .networkError, message: error.localizedDescription)
            case .denied: sendFailToDelegateWithReason(reason: .authorizationDenied, message: error.localizedDescription)
            default: sendFailToDelegateWithReason(reason: .unknown, message: error.localizedDescription)
        }
    }

    //MARK: Helpers
    func useCachedLocation(loc: CLLocation) {
        lastLocation = loc
        sendLocationToDelegate()
    }
    
    func forceUpdate() {
        forceUpdateAtualLocation = true
        requireLocation()
    }
}
