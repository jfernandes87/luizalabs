//
//  ListViewController.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import CoreLocation

class ListViewController: UIBaseViewController, CollectionManagerDelegate {

    @IBOutlet weak var collectionView: CollectionManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.managerDelegate = self
        collectionView.defaultEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 0)
    }
    
    func loadData(items: [Weather], unit: String) {
        collectionView.items = order(items: items).flatMap({ $0.toCell(unit: unit) })
        setViewStatus(status: .presenting)
    }
    
    func order(items: [Weather]) -> [Weather] {
        
        //Caso não exista a localização do usuario, listamos de acordo com o retorno da API
        guard let location = AppHelper.shared.geoloc.lastLocation else {
            return items
        }
        
        //Caso contrario ordenamos por distancia
        return items.sorted { $0.distanceTo(location: location) < $1.distanceTo(location: location) }
    }
    
    func showError() {
        setViewStatus(status: .exceptions)
    }
}
