//
//  HomeViewController.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import CoreLocation

struct NavigationBarStruct {
    var titleMaps = "Mapa"
    var titleList = "Lista"
    var titleCelsius = "°C"
    var titleFahrenheit = "°F"
    
    func toUnits(title: String) -> Units {
        return title == titleCelsius ? Units.metric : Units.imperial
    }
    
    func currentTab(title: String) -> String {
        return title == titleList ? titleMaps : titleList
    }
}

class HomeViewController: UIBaseViewController {
    
    var navStruct = NavigationBarStruct()
    
    @IBOutlet weak var leftButton: UIBarButtonItem!
    @IBOutlet weak var rightButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var mapsController: MapsViewController?
    var listController: ListViewController?
    
    deinit {
        stopListening(notificationName: OpenWeatherManager.weatherNewValue)
        stopListening(notificationName: OpenWeatherManager.weatherError)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewStatus(status: .loading)
        leftButton.title = navStruct.titleCelsius
        rightButton.title = navStruct.titleMaps
        
        listenTo(notificationName: OpenWeatherManager.weatherNewValue, call: #selector(self.weatherNewValue))
        listenTo(notificationName: OpenWeatherManager.weatherError, call: #selector(self.exception(notification:)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppHelper.shared.geoloc.userLocationRequiredWithDelegate(delegate: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let controller = segue.destination as? MapsViewController {
            controller.delegate = self
            mapsController = controller
        }
        
        if let controller = segue.destination as? ListViewController {
            listController = controller
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
            self.setViewStatus(status: .loading, animated: true)
        }) { (context) in
            self.weatherNewValue()
            self.changeScroll(type: self.navStruct.currentTab(title: self.rightButton.title!), animted: false)
        }
    }
    
    fileprivate func changeRightButtonName() {
        if rightButton.title == navStruct.titleMaps { rightButton.title = navStruct.titleList
        } else { rightButton.title = navStruct.titleMaps }
    }
    
    fileprivate func changeLeftButtonName() {
        if leftButton.title == navStruct.titleCelsius { leftButton.title = navStruct.titleFahrenheit
        } else { leftButton.title = navStruct.titleCelsius }
    }
    
    fileprivate func changeScroll(type: String, animted: Bool) {
        var rect = scrollView.frame
        rect.origin.x = type == navStruct.titleList ? 0 : rect.size.width
        scrollView.scrollRectToVisible(rect, animated: animted)
    }
}

//MARK: OpenWeatherManager
extension HomeViewController {
    func weatherNewValue() {
        guard let collection = OpenWeatherManager.shared.weatherCollection else { return }
        guard let list = listController else { return }
        guard let mapView = mapsController else { return }
        
        list.loadData(items: collection.list, unit: leftButton.title!)
        mapView.loadData(items: collection.list, unit: leftButton.title!)
        setViewStatus(status: .presenting, animated: true)
    }
    
    func exception(notification: NSNotification) {
        guard let response = notification.object as? NSDictionary else { return }
        guard let _ = response["Message"] as? String else { return }
        guard let list = listController else { return }
        list.showError()
    }
}

//MARK: Actions
extension HomeViewController {
    @IBAction func actionRightButton() {
        changeScroll(type: rightButton.title!, animted: true)
        changeRightButtonName()
    }
    
    @IBAction func actionLeftButton() {
        changeLeftButtonName()
        
        guard let mapView = mapsController else { return }
        setViewStatus(status: .loading, animated: true)
        mapView.mapViewIdle()
    }
}

//MARK: Geoloc delegate
extension HomeViewController: GeolocDelegate {
    func geoloc(geoLoc: Geoloc, didGetUserLocation location: CLLocation) {
        guard let coordinate = geoLoc.lastLocation?.coordinate else { return }
        guard let mapView = mapsController else { return }
        
        //Posiciona o mapa na localização do usuario
        mapView.mapView(center: coordinate, animated: true)
    }
    
    func geoloc(geoLoc: Geoloc, failWithReason reason: Int, message: String) {
        let alert = UIAlertController(title: "Luizalabs", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert) in self.dismiss(animated: true, completion: nil) }))
        present(alert, animated: true, completion: nil)
    }
}

//MARK: Mapview delegate
extension HomeViewController: MapsViewControllerDelegate {
    
    //Chamada para API
    func mapView(bounds northEast: (lat: Double, lon: Double), southWest: (lat: Double, lon: Double), zoom: Int) {
        OpenWeatherManager.shared.loadWeatherWithBounds(northEast: (northEast.lat, northEast.lon),
                                                        southWest: (southWest.lat, southWest.lon),
                                                        zoom: zoom,
                                                        units: navStruct.toUnits(title: leftButton.title!))
    }
}
