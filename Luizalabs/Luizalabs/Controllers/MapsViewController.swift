//
//  MapsViewController.swift
//  Luizalabs
//
//  Created by Julio Fernandes on 19/04/17.
//  Copyright © 2017 Julio Fernandes. All rights reserved.
//

import UIKit
import MapKit

protocol MapsViewControllerDelegate: NSObjectProtocol {
    func mapView(bounds northEast: (lat: Double, lon: Double), southWest: (lat: Double, lon: Double), zoom: Int)
}

class MapsViewController: UIBaseViewController {
    
    weak var delegate: MapsViewControllerDelegate?
    var mapViewIdleDelay = Timer()
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    
    func mapView(center coordinate: CLLocationCoordinate2D, animated: Bool) {
        mapView.setRegion(region(coordinate: coordinate), animated: true)
    }
    
    func loadData(items: [Weather], unit: String) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(items.flatMap({$0.toAnnotation(unit: unit)}))
    }
    
    fileprivate func region(coordinate: CLLocationCoordinate2D) -> MKCoordinateRegion {
         var region = MKCoordinateRegion()
        region.center = coordinate
        region.span.longitudeDelta  = 0.1
        region.span.latitudeDelta  = 0.1
        
        return region
    }
}

//MARK: Mapview delegate
extension MapsViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Annotation {
            
            let identifier = "pin"
            var view: MKPinAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                imageView.setURL(url: annotation.image, cache: true)
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.leftCalloutAccessoryView = imageView
                view.canShowCallout = true
            }
            
            return view
        }
        
        return nil
    }
    
    /*
     * Obs: Vamos deixar um delay porque o usuario pode mexer o mapa diversas vezes e so queremos realmente a ultima posição do mapa.
     */
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        mapViewIdleDelay.invalidate()
        mapViewIdleDelay = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.mapViewIdle), userInfo: [:], repeats: false)
    }
    
    func mapViewIdle() {
        let northEast = mapView.convert(CGPoint(x: mapView.bounds.width, y: 0), toCoordinateFrom: mapView)
        let southWest = mapView.convert(CGPoint(x: 0, y: mapView.bounds.height), toCoordinateFrom: mapView)
        let zoom = mapView.zoom()

        if let delegate = delegate {
            delegate.mapView(bounds: (northEast.latitude, northEast.longitude), southWest: (southWest.latitude, southWest.longitude), zoom: zoom)
        }
    }

}
